const button = document.querySelector(".button");
// console.log(button);
function initialState(themaName) {
localStorage.setItem('thema', themaName);
document.documentElement.className = themaName;

}

function toggleThema() {
    if (localStorage.getItem('thema') == 'dark-thema'){
    initialState('light-thema')
    } else {
    initialState('dark-thema')
    }
}

button.addEventListener('click', (e) => {
toggleThema();
})